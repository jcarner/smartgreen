#! /usr/bin/python

# Derived from code by Alejandro Andreu
import commands
import json
import serial
import time
import os
from serial import SerialException
from xbee import ZigBee

print 'Asynchronously printing data from remote XBee'

serial_port = serial.Serial('COM4', 9600)

def print_data(data):
    """
    This method is called whenever data is received.
    Its only argument is the data within the frame.
    """
    fs=data['rf_data']				 #creates string with data from arduino
    h=fs[3:8] 						#creates substring with humidity
    t=fs[12:17]						#temperature
    hi=fs[22:27]					#heat index
    l=fs[31:34]						#light

    # Create a JSON file and fill it with the received samples
    json_data = {}
    json_data['version'] = '0.2'
    json_data['datastreams'] = ()
    
    #add all the information on the JSON with its ID
    
    json_data['datastreams'] = json_data['datastreams'] + ({'id': 'Humidity', 'current_value': h},)
    json_data['datastreams'] = json_data['datastreams'] + ({'id': 'Temperature', 'current_value': t},)    
    json_data['datastreams'] = json_data['datastreams'] + ({'id': 'HeatIndex', 'current_value': hi},)    
    json_data['datastreams'] = json_data['datastreams'] + ({'id': 'Light', 'current_value': l},)
    
    print json_data  #look at the pretty data!
    
    # Creates JSON called Xbee2Xively and dumps all the datastreams inside of it
    
    with open('Xbee2Xively.json', mode='w') as f:
        json.dump(json_data, f, indent = 4)
    
    # Upload JSON to Xively using our curl to send the PUT request with our API key and feed ID
    os.popen('curl --request PUT --data-binary @Xbee2Xively.json --header "X-ApiKey:n4XwrgcoqpQqVdyMFV2K4XzK8wfloAQgSR33jPhOZcX4EmZB" --verbose https://api.xively.com/v2/feeds/1267864406')
 
 # calls the port where our coordinator is, gets the data and calls print_data so we can see the info
zigbee = ZigBee(serial_port, escaped = True, callback = print_data) 

#I'm sleepy

time.sleep(300)

zigbee.halt();
serial_port.close()